######################################
#                                    #
# Prueba técnica NTTData             #
# Versión utilizadas para la prueba  #
# Npm 8.5.1                          #
# Nodjs 12.22.9                      #
#                                    #
######################################

####### Inicializar la prueba #######

Para iniciar la navegación de la página, es necesario se cree una cookie, para lo que debemos cambiarlo 
cada ejecución que se realice en las pruebas.

cy.setCookie('user', 'e4664419-361d-e4d9-fa26-3157cbe284f2')

sepuede modificar un número para que sea un cookie diferente

Desde la consola ejecutamos la sentencia
npm run cy:open